module.exports = function (app, client) {
    function makeid()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    
        for( var i=0; i < 5; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
    
        return text;
    }
    
    app.get('/', function (req, res) {
        res.render('index', { title : 'Создать кошелек онлайн'}); 
    });
    
    app.get('/wallet/:wallet', function (req, res) {
        var wallet = req.params.wallet;
        client.cmd('getaccount', wallet, function (err, acc) {
           if (err || !acc) {
               if (err) {
                   console.log(err);
               }
               
               return res.redirect('/');
           } else {
               client.cmd('getbalance', acc, function (err, balance) {
                  if (err) {
                      console.log(err);
                      return res.redirect('/');
                  } else {
                      client.cmd('listtransactions', acc, function (err, transactions) {
                          if (err) {
                              console.log(err);
                              return res.reidrect('/');
                          } else {
                              for (var i = 0; i < transactions.length; i++) {
                                  transactions[i].time = new Date(transactions[i].time * 1000);
                              }
                              res.render('wallet', { title : 'Ваш кошелек', wallet : wallet, balance : balance, transactions : transactions });
                          }
                      });
                  }
               });
           }
        });
    });
    
    app.post('/getbalance', function (req, res) {
       var wallet = req.body.wallet;
       client.cmd('getaccount', wallet, function (err, acc) {
           if (err || !acc) {
               if (err) {
                   console.log(err);
               }
               
               return res.json({ success : false });
           } else {
               client.cmd('getbalance', acc, function (err, balance) {
                  if (err) {
                      console.log(err);
                      return res.json({ success : false });
                  }  else {
                      return res.json({ success : true, balance : balance });
                  }
               });
           }
       });
    });
    
    app.post('/sendBitcoin', function (req, res) {
        var wallet = req.body.wallet;
        var amount = req.body.amount;
        var addr = req.body.addr;
        
        amount = parseFloat(amount);
        
        if (!addr || addr.length == 0) {
            return res.json({ success : false });
        }
        
        if (isNaN(amount)) {
            return res.json({ success : false });
        }
        
        client.cmd('getaccount', wallet, function (err, acc) {
           if (err || !acc) {
               if (err) {
                   console.log(err);
               }
               
               return res.json({ success : false });
           } else {
               client.cmd('getbalance', acc, function (err, balance) {
                  if (err) {
                      console.log(err);
                      return res.json({ success : false });
                  } else {
                      if (amount > balance) {
                          return res.json({ success : false });
                      } else {
                          client.cmd('sendfrom', acc, addr, amount, function (err, tid) {
                             if (err) {
                                 console.log(err);
                                 return res.json({ success : false });
                             } else {
                                 return res.json({ success : true, tid : tid });
                             }
                          });
                      }
                  }
               });
           }
       });
    });
    
    app.post('/newwallet', function (req, res) {
        var id = makeid();
        client.cmd('getnewaddress', id, function (err, addr) {
           if (err) {
               console.log(err);
               return res.json({ success : false });
           } else {
               return res.json({ success : true, addr : addr });
           }
        });
    });
}
